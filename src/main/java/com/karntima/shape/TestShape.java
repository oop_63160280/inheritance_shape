/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.karntima.shape;

/**
 *
 * @author User
 */
public class TestShape {
    public static void main(String[] args) {
        Shape shape = new Shape();
        shape.calArea();
        shape.print();
        
        Circle circle = new Circle(2);
        circle.calArea();
        circle.print();
        
        Rectangle rectangle = new Rectangle(2,3);
        rectangle.calArea();
        rectangle.print();
             
        Square square = new Square(3);
        square.calArea();
        square.print();
        
        Triangle triangle = new Triangle(4,2);
        triangle.calArea();
        triangle.print();
        
        System.out.println("Circle is Shape = " + (circle instanceof Shape));
        System.out.println("Rectangle is Shape = " + (rectangle instanceof Shape));
        System.out.println("Rectangle is Square = " + (rectangle instanceof Square));
        System.out.println("Square is Shape = " + (square instanceof Shape));
        System.out.println("Triangle is Shape = " + (triangle instanceof Shape));
        System.out.println("Shape is Triangle = " + (shape instanceof Triangle));
        
        Shape[] shapee = {circle,rectangle,triangle,square};
        for(int i=0;i<shapee.length;i++){
            shapee[i].calArea();
            shapee[i].print();
            System.out.println("-------");
        }
    }
}
