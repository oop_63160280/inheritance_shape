/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.karntima.shape;

/**
 *
 * @author User
 */
public class Circle extends Shape{
    private double r;
    private static final double pi = 22.0/7;

    public Circle(double r) {
        System.out.println("Create Circle");
        this.r = r;
    }
    
    public double calArea(){
        return r*r*pi;
    }
    
    public void print(){
        System.out.println("r: "+this.r+" Area circle: "+calArea());
    }
}
