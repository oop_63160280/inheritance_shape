/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.karntima.shape;

/**
 *
 * @author User
 */
public class Triangle extends Shape{

    private double base;
    private double height;

    public Triangle(double base, double height) {
        System.out.println("Create Triangle");
        this.base = base;
        this.height = height;
    }

    public double calArea() {
        return 1.0 / 2 * base * height;
    }
    
    public void print(){
        System.out.println("base: "+ this.base+" height: "+this.height+
                           " Area triangle: "+ calArea());
    }
}
