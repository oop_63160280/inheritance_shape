/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.karntima.shape;

/**
 *
 * @author User
 */
class Rectangle extends Shape{
    private double width;
    private double height;

    public Rectangle(double width, double height) {
        System.out.println("Create Rectangle");
        this.width = width;
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }
    
    public void print(){
        if(width<=0 || height <=0){
            System.out.println("Error: wide must more than zero!!!");
            return ;
        }else{
            System.out.println("width: "+this.width+" heigth: "
                                +this.height +" Area rectangle: "+ calArea());
        }
        this.height = height;
        this.width = width;
    }
    
    public double calArea(){
        return width*height;
    }
    
    
}
